/*global  $*/
$(document).ready(function () {
    $(".images").slick({
        dots: true,
        infinite: true,
        arrow: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 10000,
        responsive: [{
            breakpoint: 720,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
